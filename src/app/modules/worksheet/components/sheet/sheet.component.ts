import { Component, OnInit } from '@angular/core';
import { Question } from 'src/app/modules/interface';
import { WorksheetService } from '../../worksheet.service';
import {CdkDragDrop, copyArrayItem, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-sheet',
  templateUrl: './sheet.component.html',
  styleUrls: ['./sheet.component.scss']
})
export class SheetComponent implements OnInit {
  ques = <Question[]>[];

  constructor( private service : WorksheetService) { }

  ngOnInit(): void {
    // console.log("================= style data==================",this.service.getBank());
    this.service.newQuestion.subscribe(
      (data : Question) => {
         this.addNewQuestion(data);
      }
    )
  }
  addNewQuestion(question : Question) {
    console.log("============question=============",question);
    this.ques.push(question);
  }
  onDrop(event: CdkDragDrop<Question[]>) {
    console.log("=============event====sheet===========",event);
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      event.previousContainer.data[event.previousIndex].status = true;
      copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
  }
}
remove(index : number) {
   this.ques[index].status = false;
   this.ques.splice(index,1);
   console.log("============questions=========",this.ques)
}

}
