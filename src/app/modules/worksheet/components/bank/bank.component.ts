import { Component, OnInit } from '@angular/core';
import { Question } from 'src/app/modules/interface';
import { WorksheetService } from '../../worksheet.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {
  bank = <Question[]>[];
  constructor(private service : WorksheetService) { }

  ngOnInit(): void {
    // console.log("================= style data==================",this.service.getBank());
    this.bank = this.service.getBank();
  }
  onDrop(event: CdkDragDrop<Question[]>) {
    console.log("=============event===============",event);
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
  }
}
addQuestionToSheet(index : number){
  console.log("==================index==============",this.bank[index])
  this.bank[index].status = true;
  this.service.newQuestion.next(this.bank[index])
}

}
