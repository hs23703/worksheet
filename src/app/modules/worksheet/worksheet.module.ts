import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SheetComponent } from './components/sheet/sheet.component';
import { BankComponent } from './components/bank//bank.component';
import { WorksheetComponent } from './worksheet.component';
import { MatSliderModule } from '@angular/material/slider';
import { RouterModule } from '@angular/router';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatButtonModule} from '@angular/material/button';


const routes = [ 
  {path : '', component: WorksheetComponent},
  ];

@NgModule({
  declarations: [SheetComponent, BankComponent, WorksheetComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatSliderModule,
    DragDropModule,
    MatButtonModule
  ],
  bootstrap: [WorksheetComponent]
})
export class WorksheetModule { 
  constructor() {
    console.log("=============module===============");
  }
}
