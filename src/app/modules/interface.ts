export interface Question {
    statement: string;
    type : number;
    status : boolean;
}