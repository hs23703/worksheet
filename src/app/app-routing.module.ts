import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "worksheet",
    loadChildren: () => import('./modules/worksheet/worksheet.module').then(m => m.WorksheetModule)
  },
  { path: '',   redirectTo: 'worksheet', pathMatch: 'full' },
  { path: '**', redirectTo: 'worksheet' }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
